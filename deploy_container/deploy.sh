#!/bin/bash

#####################################################################
#
# Description: déploiement à la volée de conteneur docker debian
# Auteur: Benoit Petit
# Date: 11/07/2020
#
#####################################################################


# --------------------------------------------------------
# créer les conteneurs
if [ "$1" == "--create" ];then
  # nombre de machine par default == 1
  nb_machine=1
  [ "$2" != "" ] && nb_machine=$2
  # recuperation de l'id max
  min=1
  max=0
  idmax=`docker ps -a --format '{{ .Names}}' | awk -F "-" -v user="$USER" '$0 ~ user"-debian" {print $3}' | sort -r |head -1`
  min=$(($idmax + 1))
  max=$(($idmax + $nb_machine))
  echo "Début de la création du/des conteneur(s)..."

  # lancement des conteneurs
	for i in $(seq $min $max);do
		docker run -tid --cap-add NET_ADMIN --cap-add SYS_ADMIN --publish-all=true -v /srv/data:/srv/html -v /sys/fs/cgroup:/sys/fs/cgroup:ro --name $USER-debian-$i -h $USER-debian-$i registry.gitlab.com/benoitpetit/scripts:latest
		docker exec -ti $USER-debian-$i bash -c "useradd -m -p sa3tHJ3/KuYvI $USER"
		docker exec -ti $USER-debian-$i bash -c "mkdir  ${HOME}/.ssh && chmod 700 ${HOME}/.ssh && chown $USER:$USER $HOME/.ssh"
    docker cp $HOME/.ssh/id_rsa.pub $USER-debian-$i:$HOME/.ssh/authorized_keys
    docker exec -ti $USER-debian-$i bash -c "chmod 600 ${HOME}/.ssh/authorized_keys && chown $USER:$USER $HOME/.ssh/authorized_keys"
		docker exec -ti $USER-debian-$i bash -c "echo '$USER   ALL=(ALL) NOPASSWD: ALL'>>/etc/sudoers"
		docker exec -ti $USER-debian-$i bash -c "service ssh start"
		echo -e "Conteneur $USER-debian-$i créé"
	done
  echo -e "\e[32m------------------------------------\e[0m"
	for conteneur in $(docker ps -a | grep $USER-debian | awk '{print $1}');do
      docker inspect -f '    [{{.Config.Hostname}}] : {{.NetworkSettings.IPAddress }}' $conteneur
	done
  echo -e "\e[32m------------------------------------\e[0m"


# --------------------------------------------------------
# supprimer les conteneurs
elif [ "$1" == "--drop" ]; then
  echo "Suppression du/des conteneur(s)... "
  docker rm -f $(docker ps -a | grep $USER-debian | awk '{print $1}')
  echo -e "\e[0m[ \e[32mOK\e[0m ]"

# --------------------------------------------------------
# start les conteneurs
elif [ "$1" == "--start" ]; then
  echo "Redémarrage du/des conteneur(s)..."
  docker start $(docker ps -a | grep $USER-debian | awk '{print $1}')
  echo -e "\e[0m[ \e[32mOK\e[0m ]"

# --------------------------------------------------------
# stop les conteneurs
elif [ "$1" == "--stop" ]; then
  echo "Arret du/des conteneur(s)..."
  docker stop $(docker ps -q)
  echo -e "\e[0m[ \e[32mOK\e[0m ]"

# --------------------------------------------------------
# Informations sur les conteneurs
elif [ "$1" == "--infos" ];then

  echo ""
  echo "Informations du/des conteneur(s) : "
  echo -e "\e[32m"
	for conteneur in $(docker ps -a | grep $USER-debian | awk '{print $1}');do
      docker inspect -f '           [{{.Config.Hostname}}] : {{.NetworkSettings.IPAddress }}' $conteneur
	done
	echo ""

else
echo "
:::::::::  :::::::::: :::::::::  :::         ::::::::  :::   :::
:+:    :+: :+:        :+:    :+: :+:        :+:    :+: :+:   :+:
+:+    +:+ +:+        +:+    +:+ +:+        +:+    +:+  +:+ +:+
+#+    +:+ +#++:++#   +#++:++#+  +#+        +#+    +:+   +#++:
+#+    +#+ +#+        +#+        +#+        +#+    +#+    +#+
#+#    #+# #+#        #+#        #+#        #+#    #+#    #+#
#########  ########## ###        ##########  ########     ###

-----------------------------------------------------------------
          Déploiement à la volée de conteneur docker
-----------------------------------------------------------------

Options :
          --create : Lancer des conteneurs

          --drop : Supprimer les conteneurs créer par le script

          --infos : Caractéristique du/des conteneur(s) (ip, nom)

          --start : Redémarrage du/des conteneur(s)

          --stop : Arret du/des conteneur(s)...

-----------------------------------------------------------------
"
fi
